﻿using System;

namespace ZADATAK_7
{
    class Program
    {
        static void Main(string[] args)
        {
            IBuilder builder = new NotificationBuilder();
            string author = "Computer";
            Director director = new Director();
            director.ConstructINFO(author, builder);
            ConsoleNotification message = builder.Build();
            NotificationManager manager = new NotificationManager();
            manager.Display(message);
            ConsoleNotification cloneNotification = (ConsoleNotification)message.Clone();
            manager.Display(cloneNotification);
        }
    }
}
