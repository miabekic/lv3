﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    interface Prototype
    {
        Prototype Clone();
    }
}
