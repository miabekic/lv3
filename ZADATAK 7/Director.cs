﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZADATAK_7
{
    class Director
    {
        public void ConstructINFO(String author, IBuilder builder)
        {
            builder.SetAuthor(author).SetLevel(Category.INFO).SetText("Today is a sunny day").SetTime(DateTime.Today).SetTitle("Weather cack").SetColor(ConsoleColor.Green);
        }
        public void ConstructALERT(String author, IBuilder builder)
        {
            builder.SetAuthor(author).SetLevel(Category.ALERT).SetText("Watch what are you doing!").SetTime(DateTime.Now).SetTitle("WARNING!").SetColor(ConsoleColor.Yellow);  
        }
        public void ConstructERROR(String author, IBuilder builder)
        {
            builder.SetAuthor(author).SetLevel(Category.ERROR).SetText("Something is wrong").SetTime(DateTime.Now).SetTitle("Warning!").SetColor(ConsoleColor.Red);
        }
    }
}
