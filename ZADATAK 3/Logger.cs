﻿using System;
using System.Collections.Generic;
using System.Text;

// logger ce pisati u istu datoteku ako se ponovno koristi negdje dalje u tekstu, ako se nova nije postavila
namespace ZADATAK_3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;
        private Logger()
        {
            filePath = @"C:\Users\Mia\Desktop\Fakultet\RPPOON\LV3\ZADATAK 3\LogingFile.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(message);
            }
        }
        public void setFilePath(string FilePath)
        {
            filePath = FilePath;
        }
    }
}
