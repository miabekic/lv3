﻿using System;

namespace ZADATAK_6
{
    class Program
    {
        static void Main(string[] args)
        {
            IBuilder builder = new NotificationBuilder();
            string author = "Mia";
            Director director = new Director();
            director.ConstructINFO(author, builder);
            ConsoleNotification notification = builder.Build();
            NotificationManager manager = new NotificationManager();
            manager.Display(notification);
        }
    }
}
