﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZADATAK_6
{
    class NotificationBuilder:IBuilder
    {
        private String Author="Computer";
        private String Title="Mistake!";
        private DateTime Timestamp = DateTime.Now;
        private Category Level = Category.ERROR;
        private ConsoleColor Color=ConsoleColor.Red;
        private String Text="Something is wrong!";

        public NotificationBuilder() { }
        public ConsoleNotification Build()
        {
            return new ConsoleNotification(Author, Title, Text, Timestamp, Level, Color);
        }
        public IBuilder SetAuthor(String author)
        {
            Author = author;
            return this;
        }

        public IBuilder SetTitle(String title)
        {
            Title = title;
            return this;
        }
        public IBuilder SetTime(DateTime time)
        {
            Timestamp = time;
            return this;
        }
        public IBuilder SetLevel(Category level)
        {
            Level = level;
            return this;
        }
        public IBuilder SetColor(ConsoleColor color)
        {
            Color = color;
            return this;
        }
        public IBuilder SetText(String text)
        {
            Text = text;
            return this;
        }



    }
}
