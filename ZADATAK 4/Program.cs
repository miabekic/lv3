﻿using System;

namespace ZADATAK_4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification message = new ConsoleNotification("Mia", "Life", "We all got expectations and sometimes they go wrong", DateTime.Now, Category.ALERT, ConsoleColor.Yellow);
            NotificationManager manager = new NotificationManager();
            manager.Display(message);
        }
    }
}
