﻿using System;

namespace ZADATAK_2
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator generator = MatrixGenerator.GetInstance();
            double[][] matrix = generator.generateRandomMatrix(2, 3);
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.WriteLine(matrix[i][j]);
                }
            }
        }
    }
}
