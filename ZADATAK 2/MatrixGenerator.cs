﻿using System;
using System.Collections.Generic;
using System.Text;

// stvorena metoda ima dvije odgovrnosti, stvaranje matrice i njeno popunjavanje random brojevima
namespace ZADATAK_2
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random randomGenerator;

        private MatrixGenerator()
        {
            randomGenerator = new Random();
        }
        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }
        public double[][] generateRandomMatrix(int row, int colums)
        {
            double[][] matrix = new double[row][];
            for (int i = 0; i < row; i++)
            {
                matrix[i] = new double[colums];
                for (int j = 0; j < colums; j++)
                {
                    matrix[i][j] = randomGenerator.NextDouble();
                }
            }
            return matrix;
        }
    }
}

