﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_1
{
    interface Prototype
    {
        Prototype Clone();
    }
}
