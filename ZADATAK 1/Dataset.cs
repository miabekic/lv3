﻿using System;
using System.Collections.Generic;
using System.Text;

// Potrebno je duboko kopiranje, zato što imamo reference type podatak(listu)
namespace ZADATAK_1
{
    class Dataset:Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(Dataset DataSet)
        {
            data = new List<List<string>>(DataSet.GetData().Count);
            foreach (List<string> set in DataSet.GetData())
            {
                List<string> insideDataSet = new List<string>(set.Count);
                foreach (string Data in set)
                {
                    string newData = Data;
                    insideDataSet.Add(newData);
                }
                data.Add(insideDataSet);
            }
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }

        public Prototype Clone()
        {
            Prototype dataClone = new Dataset(this);
            return dataClone;

        }

        public string printData()
        {
            StringBuilder builder = new StringBuilder();
            foreach (List<string> Data in data)
            {
                foreach (string date in Data)
                {
                    builder.Append(date);
                }

                builder.Append("\n");
            }
            return builder.ToString();
        }
    }
}

