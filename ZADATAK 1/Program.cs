﻿using System;

namespace ZADATAK_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataSet = new Dataset(@"C:\Users\Mia\Desktop\Fakultet\RPPOON\LV3\ZADATAK 1\CSV.txt");
            Console.WriteLine(dataSet.printData() + "\n");
            Dataset cloneDataSet = new Dataset();
            cloneDataSet = (Dataset)dataSet.Clone();
            Console.WriteLine(cloneDataSet.printData());
        }
    }
}
