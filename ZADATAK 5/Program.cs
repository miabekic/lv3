﻿using System;

namespace ZADATAK_5
{
    class Program
    {
        static void Main(string[] args)
        {
            IBuilder builder = new NotificationBuilder();
            NotificationManager manager = new NotificationManager();
            manager.Display(builder.Build());
            builder.SetAuthor("Mia");
            builder.SetText("You should not do that");
            builder.SetLevel(Category.ALERT);
            builder.SetTime(DateTime.Now);
            builder.SetTitle("Warning!");
            builder.SetColor(ConsoleColor.Yellow);
            manager.Display(builder.Build());

        }
    }
}
